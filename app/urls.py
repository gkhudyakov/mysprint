from django.conf.urls import patterns, url

from app import views

urlpatterns = patterns('',
    # V I E W S
    
    url(r'^$', views.index, name='index'),
    #/app/sprint-list
    url(r'^sprint-list$', views.sprint_list, name='sprint_list'),
    #/app/sprint/3/
    url(r'^sprint/(?P<sprint_id>\d+)/$', views.sprint_detail, name='sprint_detail'),
    #/app/sprint/3/edit
    url(r'^sprint/(?P<sprint_id>\d+)/edit$', views.sprint_edit, name='sprint_edit'),
    #app/sprint/new
    url(r'^sprint/new$', views.sprint_edit, name='sprint_edit'),
    
    #/app/task-list
    url(r'^task-list$', views.task_list, name='task_list'),
    #app/task/3/
    url(r'^task/(?P<task_id>\d+)/$', views.task_edit, name='task_edit'),
    #app/task/new
    url(r'^task/new/$', views.task_edit, name='task_edit'),

    
    # A P I
	
	url(r'^api/save-task/(?P<task_id>\d+)$', views.save_task, name='save_task'),
	url(r'^api/save-task/$', views.save_task, name='save_task'),

	url(r'^api/save-sprint/(?P<sprint_id>\d+)$', views.save_sprint, name='save_sprint'),
	url(r'^api/save-sprint/$', views.save_sprint, name='save_sprint'),

	url(r'^api/delete-task/$', views.delete_task, name='delete_task'),
    url(r'^api/delete-sprint/$', views.delete_sprint, name='delete_sprint'),


)