from django.shortcuts import render

from django.http import HttpResponse, HttpResponseRedirect

from app.models import Sprint, Task, TaskForm, SprintForm

# V I E W S

def index(request):
	return render (request, 'app/index.html', {})

def sprint_list(request):
	sprint_list = Sprint.objects.order_by('-id')
	return render (request, 'app/sprint-list.html', {"sprint_list": sprint_list})

def task_list(request):
	task_list = Task.objects.order_by('-id')
	return render (request, 'app/task-list.html', {"task_list": task_list})

def sprint_detail(request, sprint_id):
	sprint = Sprint.objects.get(id=sprint_id)
	return render (request, 'app/sprint-detail.html', {"sprint": sprint})

def sprint_edit(request, sprint_id=None):

	if sprint_id:
		sprint = Sprint.objects.get(id=sprint_id)
		form = SprintForm(instance=sprint)
	else:
		form = SprintForm()

	context =  {
	"form": form,
	"sprint_id": sprint_id
	}

	return render(request, 'app/sprint-edit.html', context)


def task_edit(request, task_id=None):

	modal_flag = (request.POST.get("modalFlag") == "true")
	sprint_id = request.POST.get("sprintID")

	if task_id:
		task = Task.objects.get(id=task_id)
		form = TaskForm(instance=task)
	else:
		form = TaskForm(initial={'sprint': sprint_id})

	context =  {
	"form": form,
	"task_id": task_id
	}

	if modal_flag:
		context["base_template"] = "app/task-edit.html"
		return render(request, 'app/modalWindowBase.html', context)
	else:
		return render(request, 'app/task-edit.html', context)

# A P I

def save_task(request, task_id=None):
	if request.method == 'POST':
		if task_id:
			task = Task.objects.get(id=task_id)
			form = TaskForm(request.POST, instance=task)
		else:
			form = TaskForm(request.POST)
		
		newTask = form.save()

		context =  {
			"form": form,
			"task_id": newTask.id
		}

		return HttpResponseRedirect('/app/task/' + str(newTask.id))

def save_sprint(request, sprint_id=None):
	if request.method == 'POST':
		if sprint_id:
			sprint = Sprint.objects.get(id=sprint_id)
			form = SprintForm(request.POST, instance=sprint)
		else:
			form = SprintForm(request.POST)
		
		newSprint = form.save()

		context =  {
			"form": form,
			"sprint_id": newSprint.id
		}

		return HttpResponseRedirect('/app/sprint/' + str(newSprint.id))

def delete_task(request):
	task_id = request.POST.get("id")
	if task_id != None:
		
		try:
			task = Task.objects.get(id=task_id)
		except Task.DoesNotExist:
			return HttpResponse("Error There is no task with id: " + task_id)
		
		task.delete()
		
		return HttpResponse("Task " + task_id + " successfully deleted!")
	
	else:
		return HttpResponse("Error: task id expected")

def delete_sprint(request):
	sprint_id = request.POST.get("id")
	if sprint_id != None:
		
		try:
			sprint = Sprint.objects.get(id=sprint_id)
		except sprint.DoesNotExist:
			return HttpResponse("Error There is no sprint with id: " + sprint_id)
		
		sprint.delete()
		
		return HttpResponse("sprint " + sprint_id + " successfully deleted!")
	
	else:
		return HttpResponse("Error: sprint id expected")