from django.db import models

from django.forms import ModelForm

from django.db.models import Sum


class Sprint(models.Model):
	
	NEW = 'NEW'
	STARTED = 'STARTED'
	COMPLETED = 'COMPLETED'

	SPRINT_STATUS_CHOICES = (
        (NEW, 'NEW'),
        (STARTED, 'STARTED'),
        (COMPLETED, 'COMPLETED'),
    )

	sprint_name = models.CharField(max_length=80)
	date_begin = models.DateField('start date')
	date_end = models.DateField('end date')
	duration = models.DecimalField(max_digits=4, decimal_places=1, default=1)
	status = models.CharField(max_length=9, choices=SPRINT_STATUS_CHOICES, default=NEW)

	def __str__(self):
		return self.sprint_name

	def complete_ratio(self):
		task_list = Task.objects.filter(sprint=self.id)
		estimated_sum = task_list.aggregate(Sum('estimated_time')).get('estimated_time__sum')
		elapsed_time = task_list.aggregate(Sum('elapsed_time')).get('elapsed_time__sum')
		return  str(elapsed_time) + ' / ' + str(estimated_sum)

	def complete_ratio_percent(self):
		task_list = Task.objects.filter(sprint=self.id)
		estimated_sum = task_list.aggregate(Sum('estimated_time')).get('estimated_time__sum')
		elapsed_time = task_list.aggregate(Sum('elapsed_time')).get('elapsed_time__sum')
		if elapsed_time and estimated_sum:
			return  elapsed_time / estimated_sum * 100
		else:
			return ""

	def task_total_estimated_time(self):
		task_list = Task.objects.filter(sprint=self.id)
		estimated_sum = task_list.aggregate(Sum('estimated_time')).get('estimated_time__sum')
		return str(estimated_sum)
			

class SprintForm(ModelForm):
	class Meta:
		model = Sprint
		fields = ['sprint_name','date_begin', 'date_end', 'duration', 'status']		


class Task(models.Model):
	sprint = models.ForeignKey(Sprint)
	task_name = models.CharField(max_length=80)
	description = models.TextField(max_length=400)
	estimated_time = models.DecimalField(max_digits=3, decimal_places=1)
	elapsed_time = models.DecimalField(max_digits=3, decimal_places=1, default=0)
	done = models.BooleanField(default=False)

	def __str__(self):
		return self.task_name

class TaskForm(ModelForm):
	class Meta:
		model = Task
		fields = ['sprint','task_name', 'description', 'estimated_time', 'elapsed_time', 'done']