from django.contrib import admin

from app.models import Sprint, Task

class TaskInline(admin.TabularInline):
	model = Task
	extra = 1

class SprintAdmin(admin.ModelAdmin):
	inlines = [TaskInline]
	list_display = ('sprint_name', 'date_begin', 'date_end', 'complete_ratio')
	search_fields = ['sprint_name']

class TaskAdmin(admin.ModelAdmin):
	search_fields = ['task_name']

admin.site.register(Sprint, SprintAdmin)
admin.site.register(Task, TaskAdmin)
