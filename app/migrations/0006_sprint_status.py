# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_sprint_duration'),
    ]

    operations = [
        migrations.AddField(
            model_name='sprint',
            name='status',
            field=models.CharField(default=b'NEW', max_length=9, choices=[(b'NEW', b'NEW'), (b'STARTED', b'STARTED'), (b'COMPLETED', b'COMPLETED')]),
            preserve_default=True,
        ),
    ]
