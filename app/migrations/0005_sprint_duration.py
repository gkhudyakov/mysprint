# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20151001_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='sprint',
            name='duration',
            field=models.DecimalField(default=1, max_digits=4, decimal_places=1),
            preserve_default=True,
        ),
    ]
