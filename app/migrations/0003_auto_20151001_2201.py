# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20151001_2120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='elapsed_time',
            field=models.DecimalField(max_digits=3, decimal_places=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='task',
            name='estimated_time',
            field=models.DecimalField(max_digits=3, decimal_places=1),
            preserve_default=True,
        ),
    ]
