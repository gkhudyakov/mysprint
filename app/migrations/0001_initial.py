# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sprint_name', models.CharField(max_length=80)),
                ('date_begin', models.DateField(verbose_name=b'start date')),
                ('date_end', models.DateField(verbose_name=b'end date')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_name', models.CharField(max_length=80)),
                ('description', models.CharField(max_length=400)),
                ('estimated_time', models.TimeField()),
                ('elapsed_time', models.TimeField()),
                ('done', models.BooleanField(default=False)),
                ('sprint', models.ForeignKey(to='app.Sprint')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
