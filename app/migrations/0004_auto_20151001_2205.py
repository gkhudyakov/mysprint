# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20151001_2201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='elapsed_time',
            field=models.DecimalField(default=0, max_digits=3, decimal_places=1),
            preserve_default=True,
        ),
    ]
