(function() {
    "use strict";

    // Cross-Site Request Forgery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    // api
    var api = {};

    api.deleteTask = function(id) {
        if (id != null) {
            var parameters = {
                "id": id
            };

            $.post('/app/api/delete-task/', parameters, function(result) {
                alert(result);
            });
        }
    }

    api.deleteSprint = function(id) {
        if (id != null) {
            var parameters = {
                "id": id
            };

            $.post('/app/api/delete-sprint/', parameters, function(result) {
                alert(result);
            });
        }
    }


    api.modalWindow = function(url, apiParameters, modalOptions){
        if (url != null){
            $.post(url, apiParameters, function(result){
                if(result){
                    $('#ModalWindow').html(result);
                    $('#ModalWindow').modal(modalOptions);
                }
            });
        }
        
    }

    api.modalTaskEdit = function(taskID, sprintID){
        if(taskID != null){
            var url = "/app/task/" + taskID + "/";
        }
        else{
            var url = "/app/task/new/";
        }
        var apiParameters = {
            "taskID": taskID,
            "modalFlag": "true",
            "sprintID": sprintID
        };
        var modalOptions = {};
        api.modalWindow(url, apiParameters, modalOptions);
    }
    window.api = api;
}());